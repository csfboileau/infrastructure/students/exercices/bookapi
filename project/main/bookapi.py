from flask import *
import json
import sys

app = Flask("BookApi")
PORT = 15555

# Repositories
ids = {'next_id': 3 }
books = {
	0:{'id':0,'title':'Dawnthief','author':'James Barclay','series':'The chronicles of the raven','year_published':2003,'rating':'veryGood'},
	1:{'id':1,'title':'Magician Apprentice','author':'Raymond Feist','series':'The Riftwar Saga','year_published':1982,'rating':'veryGood'},
	2:{'id':2,'title':'Firedrake','author':'Richard Knaak','series':'Legends of the Dragonream','year_published':2000,'rating':'veryGood'},
	3:{'id':3,'title':'Thelma la licorne','author':'Aaron Blabey','series':'N/A','year_published':2017,'rating':'amazing'} }

# API methods
@app.route("/")
def hello():
	return jsonify({'message':'A simple API with books'})

@app.route("/books")
def getAll():
	rating = request.args.get('rating')
	if rating is not None:
		return jsonify(getSpecificBooks('rating', rating))

	title = request.args.get('title')
	if title is not None:
		return jsonify(getSpecificBooks('title', title))

	return jsonify(books), 200

@app.route("/books", methods=['POST'])
def create():
	data = request.json
	title = data.get('title')
	author = data.get('author')
	series = data.get('series')
	year = data.get('year_published')
	rating = data.get('rating')
	try:
		new_book = createBook(title, author, series, year, rating)
		return jsonify(new_book), 201
	except ValueError as ve:
		return jsonify({'message':'Title and author must be provided to create a valid book'}), 422

@app.route("/books/<id>")
def getOne(id):
	try:
		results = getBookById(int(id))
		return jsonify(results), 200
	except ValueError as te:
		return jsonify({'message': 'ID must be a valid number'}), 422
	except KeyError as ke:
		return jsonify({'message': 'Specified ID does not exist'}), 404

@app.route("/books/<id>", methods=['PUT'])
def update(id):
	data = request.json
	title = data.get('title')
	author = data.get('author')
	series = data.get('series')
	year = data.get('year_published')
	rating = data.get('rating')
	try:
		updateBook(id, title, author, series, year, rating)
		return ('', 204)
	except ValueError as te:
		return jsonify({'message': 'ID must be a valid number'}), 400
	except KeyError as ke:
		return jsonify({'message': 'Specified ID does not exist'}), 404

@app.route("/books/<id>", methods=['DELETE'])
def delete(id):
	try:
		deleteBook(id)
		return ('', 204)
	except ValueError as te:
		return jsonify({'message': 'ID must be a valid number'}), 400
	except KeyError as ke:
		return jsonify({'message': 'Specified ID does not exist'}), 404

# Domain methods
def getNextId():
	next_id = ids['next_id'] + 1
	ids['next_id'] = next_id
	return next_id

def getBookById(id):
	return books[int(id)]

def getSpecificBooks(paramName, paramValue):
	results = []
	for book in books:
		if paramValue in book[paramName]:
			results.append(book)
	return results

def createBook(title, author, series, year, rating):
	id = getNextId()
	if title is not None and author is not None:
		new_book = {'id':id,'title':title,'author':author,'series':series,'year_published':year,'rating':rating}
		books[id] = new_book
		return new_book
	else:
		raise ValueError('Title or author must not be null')

def updateBook(id, title, author, series, year, rating):
	book_to_update = getBookById(id)
	if title is not None:
		book_to_update['title'] = title
	if author is not None:
		book_to_update['author'] = author
	if series is not None:
		book_to_update['series'] = series

	book_to_update['year_published'] = year

	if rating is not None:
		book_to_update['rating'] = rating

def deleteBook(id):
	del books[int(id)]

if __name__ == '__main__':
	#args = sys.argv[1:]
	#app_port = args[0]
	app.run(debug = True, host="0.0.0.0", port=PORT)
